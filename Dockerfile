FROM alpine:edge

MAINTAINER securefab (https://gitlab.com/securefab)

# ensure www-data user exists
RUN set -x \
	&& addgroup -g 82 -S www-data \
	&& adduser -u 82 -D -S -G www-data www-data \
	&& apk add apache2 apache2-http2 apache2-ssl apache2-proxy \
	&& rm /etc/apache2/conf.d/* \
	&& ln -sf /dev/stdout /var/log/apache2/access.log \
	&& ln -sf /dev/stderr /var/log/apache2/error.log

COPY httpd.conf /etc/apache2/httpd.conf
COPY conf.d /etc/apache2/conf.d
COPY ssl /etc/apache2/ssl
COPY httpd-foreground /usr/local/bin/

EXPOSE 443

HEALTHCHECK CMD wget --no-check-certificate https://localhost -q -O - > /dev/null 2>&1

STOPSIGNAL SIGTERM

CMD ["httpd-foreground"]
