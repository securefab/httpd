# Httpd durci

Configuration Httpd minimale, durcie avec différentes recommandations de sécurité (CIS, Apache, ANSSI).

## Image Docker
Une image docker est disponible : 

`
docker pull registry.gitlab.com/securefab/httpd
`

Basée sur l'image httpd [httpd](https://hub.docker.com/_/httpd) officielle.
L'image est testée avec Httpd 2.4.38.

## Déploiement 

 * Le contenu du dossier XXX doit être copier dans /etc/XXX

 * Les variables entourées de ** sont à remplacer.

 * Les certificats et dhparam (*.pem) sont à générer.

 * Modifier la configuration pour fonctionner avec votre projet

### Génération du certificat

## Auto signé (pour test uniquement)
```bash
openssl req -subj '/CN=172.17.0.1/O=company/C=FR' -x509 -newkey rsa:4096 -days 60 -nodes > site.pem
```

### Génération du dhparam
```bash
openssl dhparam -dsaparam -out dhparam.pem 4096
```
Ou alors :
```bash
 docker run openssl dhparam -dsaparam 4096
```
### Examples de configurations utiles 

#### Static resources (server)
```c
TODO
```
